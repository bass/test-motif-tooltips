/*
 * $Id$
 */
#include <stdlib.h>

#include <Xm/XmAll.h>

Widget exitButton;

const char *exitButtonForeground = "#000080";

void exitButtonArmed() {
	const char *exitButtonArmedForeground = "Blue";
	XtVaSetValues(exitButton,
			XtVaTypedArg, XmNforeground, XmRString, exitButtonArmedForeground, strlen(exitButtonArmedForeground) + 1,
			NULL);
}

void exitButtonDisarmed() {
	XtVaSetValues(exitButton,
			XtVaTypedArg, XmNforeground, XmRString, exitButtonForeground, strlen(exitButtonForeground) + 1,
			NULL);
}

int main(int argc, char *argv[]) {
	XtAppContext appContext;
	const char *applicationClass = "XmTooltipTest";
	String fallbackResources[] = {
		"?.TipShell.TipLabel.background: #fff7e9",
		NULL,
	};
	const Widget topLevel = XtVaAppInitialize(&appContext, applicationClass, NULL, 0, &argc, argv, fallbackResources,
#if XmVERSION >= 2 && XmREVISION >= 2
			XmNtoolTipEnable, True,
			XmNtoolTipPostDelay, 500,
			XmNtoolTipPostDuration, 3000,
#endif
			XmNtitle, "Tooltip Test, " XmVERSION_STRING,
			XmNiconName, "Tooltip Test, " XmVERSION_STRING " (Icon)",
			NULL);
	char *mainWindowName = "mainWindow";
	const Widget mainWindow = XmCreateMainWindow(topLevel, mainWindowName, NULL, 0);

	const XmString toolTipString = XmStringCreateLocalized("Exit the program");
	const char *exitButtonBackground = "#dcdad5";
	const char *exitButtonArmColor = "#999999";
	const char *exitButtonHighlightColor = "#666699";
	exitButton = XtVaCreateWidget("Exit", xmPushButtonWidgetClass, mainWindow,
			XtVaTypedArg, XmNbackground, XmRString, exitButtonBackground, strlen(exitButtonBackground) + 1,
			XtVaTypedArg, XmNforeground, XmRString, exitButtonForeground, strlen(exitButtonForeground) + 1,
			XtVaTypedArg, XmNarmColor, XmRString, exitButtonArmColor, strlen(exitButtonArmColor) + 1,
			XtVaTypedArg, XmNhighlightColor, XmRString, exitButtonHighlightColor, strlen(exitButtonHighlightColor) + 1,
			XmNhighlightThickness, 1,
			XmNmnemonic, 'x', // Case matters
			XmNshadowThickness, 1,
#if XmVERSION >= 2 && XmREVISION >= 2
			XmNtoolTipString, toolTipString,
#endif
			NULL);
	XmStringFree(toolTipString);

	XtAddCallback(exitButton, XmNactivateCallback, (XtCallbackProc) exit, NULL);
	XtAddCallback(exitButton, XmNarmCallback, (XtCallbackProc) exitButtonArmed, NULL);
	XtAddCallback(exitButton, XmNdisarmCallback, (XtCallbackProc) exitButtonDisarmed, NULL);
	XtManageChild(exitButton);

	XtManageChild(mainWindow);
	XtRealizeWidget(topLevel);
	XtAppMainLoop(appContext);

	return 0;
}
